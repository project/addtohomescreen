-- SUMMARY --

Invites the user to add the site to the home screen using
<a href="https://github.com/cubiq/add-to-homescreen">Add to homescreen</a>.

-- INSTALLATION --

To install the Add to homescreen library, download the project's official zip
file: https://github.com/cubiq/add-to-homescreen/archive/master.zip
Extract it, and copy the contents to a folder in your Drupal site
named libraries/addtohomescreen (you'll need to create that folder).

If you have drush, you can install Add to homescreen by running this command:
drush addtohomescreen
