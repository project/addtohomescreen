/**
 * @file
 * Call the 'Add to homescreen' library with the Drupal settings.
 */

(function (Drupal, drupalSettings, addToHomescreen) {

  Drupal.behaviors.addToHomeScreen = {
    attach: function (context, settings) {

      settings.addtohomescreen = settings.addtohomescreen || drupalSettings.addtohomescreen;
      if (typeof (addToHomescreen) === 'function') {
        if (settings.addtohomescreen.debug) {
          addToHomescreen.removeSession();
        }
        addToHomescreen(settings.addtohomescreen);
      }
    }
  };

})(Drupal, drupalSettings, addToHomescreen);
